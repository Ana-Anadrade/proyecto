import { Component } from "@angular/core";
import { RouterModule, Routes } from "@angular/router"

import { 
    CuerpopaginaComponent,
    HorarioComponent,
    ListadoComponent,
    MenuComponent,
    GeneralComponent,
    InicioComponent,
    RegistroComponent,
    RecuperarComponent,
} from "./components/index.pagina";


const app_routes: Routes= [ 
    {path:'',component:CuerpopaginaComponent},
    {path:'Agenda',component:HorarioComponent},
    {path:'Asistencia',component:ListadoComponent},
    {path:'Cambio de contraseña',component:MenuComponent},
    {path:'Perfil',component:GeneralComponent},
    {path:'Inicio',component:InicioComponent},
    {path:'Registro',component:RegistroComponent},
    {path:'Recuperar',component:RecuperarComponent},
    {path:'**',pathMatch:'full',redirectTo:''}
];
export const app_routing = RouterModule.forRoot(app_routes,{useHash:true});