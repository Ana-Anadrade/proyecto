import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
///rutas 
import { app_routing } from './app.routes';
///componentes
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { PiespaginaComponent } from './components/piespagina/piespagina.component';
import { CuerpopaginaComponent } from './components/cuerpopagina/cuerpopagina.component';
import { HorarioComponent } from './components/horario/horario.component';
import { ListadoComponent } from './components/listado/listado.component';
import { MenuComponent } from './components/menu/menu.component';
import { GeneralComponent } from './components/general/general.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { RegistroComponent } from './components/registro/registro.component';
import { RecuperarComponent } from './components/recuperar/recuperar.component';
import { JsonComponent } from './components/json/json.component';
import { HttpClientModule } from '@angular/common/http';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideFirestore,getFirestore } from '@angular/fire/firestore'


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PiespaginaComponent,
    CuerpopaginaComponent,
    HorarioComponent,
    ListadoComponent,
    MenuComponent,
    GeneralComponent,
    InicioComponent,
    RegistroComponent,
    RecuperarComponent,
    JsonComponent,
  ],
  imports: [
    BrowserModule,
    app_routing,
    HttpClientModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore())

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
