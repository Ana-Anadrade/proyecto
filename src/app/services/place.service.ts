import { Firestore ,} from '@angular/fire/firestore';
import { addDoc, collection } from 'firebase/firestore';
import place from '../interface/place.interface';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  constructor(private firestore:Firestore) { }
    addPlace(place:place){
    const placeRef = collection(this.firestore,'places')
    return addDoc(placeRef,place)
    }
}
