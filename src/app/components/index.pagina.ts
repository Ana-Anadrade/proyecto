export{ CuerpopaginaComponent } from './cuerpopagina/cuerpopagina.component';
export{ HorarioComponent } from './horario/horario.component';
export { ListadoComponent } from './listado/listado.component';
export { MenuComponent } from './menu/menu.component';
export { GeneralComponent } from './general/general.component';
export { InicioComponent } from './inicio/inicio.component';
export { RegistroComponent } from './registro/registro.component';
export  { RecuperarComponent } from './recuperar/recuperar.component';