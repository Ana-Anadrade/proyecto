import { Component } from '@angular/core';
import Registro from "src/assets/json/registro.json"
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Control';
  Registros=Registro;
  
}
